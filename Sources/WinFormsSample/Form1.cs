﻿using AForge.Controls;
using static AForge.Controls.Joystick;
using System.IO;
using AForge.Video.DirectShow;

namespace WinFormsSample
{
    public partial class Form1 : Form
    {
        private readonly VideoSourcePlayer _player;

        public Form1()
        {
            InitializeComponent();

            _player = new VideoSourcePlayer() { Dock = DockStyle.Fill };
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var devicesList = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            if (devicesList.Count == 0)
            {
                return;
            }

            var device = devicesList[0];
            _player.VideoSource = new VideoCaptureDevice(device.MonikerString);
            Controls.Add(_player);
            _player.Start();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            _player.Stop();
        }
    }
}
